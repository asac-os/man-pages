.\" Copyright (C) 2011 by Andi Kleen <andi@firstfloor.org>
.\" and Copyright (c) 2011 by Michael Kerrisk <mtk.manpages@gmail.com>
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Since the Linux kernel and libraries are constantly changing, this
.\" manual page may be incorrect or out-of-date.  The author(s) assume no
.\" responsibility for errors or omissions, or for damages resulting from
.\" the use of the information contained herein.  The author(s) may not
.\" have taken the same level of care in the production of this manual,
.\" which is licensed free of charge, as they might when working
.\" professionally.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\"
.\" FIXME: This page could be improved with an example program.
.\"
.TH RECVMMSG 2 2012-05-02 "Linux" "Linux Programmer's Manual"
.SH NAME
recvmmsg \- receive multiple messages on a socket
.SH SYNOPSIS
.nf
.B "#define _GNU_SOURCE"
.BI "#include <sys/socket.h>"

.BI "int recvmmsg(int " sockfd ", struct mmsghdr *" msgvec \
", unsigned int " vlen ","
.br
.BI "             unsigned int " flags ", struct timespec *" timeout ");"
.fi
.SH DESCRIPTION
The
.BR recvmmsg ()
system call is an extension of
.BR recvmsg (2)
that allows the caller to receive multiple messages from a socket
using a single system call.
(This has performance benefits for some applications.)
A further extension over
.BR recvmsg (2)
is support for a timeout on the receive operation.

The
.I sockfd
argument is the file descriptor of the socket to receive data from.

The
.I msgvec
argument is a pointer to an array of
.I mmsghdr
structures.
The size of this array is specified in
.IR vlen .

The
.I mmsghdr
structure is defined in
.I <sys/socket.h>
as:

.in +4n
.nf
struct mmsghdr {
    struct msghdr msg_hdr;  /* Message header */
    unsigned int  msg_len;  /* Number of received bytes for header */
};
.fi
.in
.PP
The
.I msg_hdr
field is a
.I msghdr
structure, as described in
.BR recvmsg (2).
The
.I msg_len
field is the number of bytes returned for the message in the entry.
This field has the same value as the return value of a single
.BR recvmsg (2)
on the header.

The
.I flags
argument contains flags ORed together.
The flags are the same as documented for
.BR recvmsg (2),
with the following addition:
.TP
.B MSG_WAITFORONE
Turns on
.B MSG_DONTWAIT
after the first message has been received.
.PP
The
.I timeout
argument points to a
.I struct timespec
(see
.BR clock_gettime (2))
defining a timeout (seconds plus nanoseconds) for the receive operation.
(This interval will be rounded up to the system clock granularity,
and kernel scheduling delays mean that the blocking interval
may overrun by a small amount.)
If
.I timeout
is
.I NULL
then the operation blocks indefinitely.

A blocking
.BR recvmmsg ()
call blocks until
.I vlen
messages have been received
or until the timeout expires.
A nonblocking call reads as many messages as are available
(up to the limit specified by
.IR vlen )
and returns immediately.

On return from
.BR recvmmsg (),
successive elements of
.IR msgvec
are updated to contain information about each received message:
.I msg_len
contains the size of the received message;
the subfields of
.I msg_hdr
are updated as described in
.BR recvmsg (2).
The return value of the call indicates the number of elements of
.I msgvec
that have been updated.
.SH RETURN VALUE
On success,
.BR recvmmsg ()
returns the number of messages received in
.IR msgvec ;
on error, \-1 is returned, and
.I errno
is set to indicate the error.
.SH ERRORS
Errors are as for
.BR recvmsg (2).
In addition, the following error can occur:
.TP
.B EINVAL
.I timeout
is invalid.
.SH VERSIONS
The
.BR recvmmsg ()
system call was added in Linux 2.6.32.
Support in glibc was added in version 2.12.
.SH CONFORMING TO
.BR recvmmsg ()
is Linux-specific.
.SH SEE ALSO
.BR clock_gettime (2),
.BR recvmsg (2),
.BR sendmmsg (2),
.BR sendmsg (2),
.BR socket (2),
.BR socket (7)
.SH COLOPHON
This page is part of release 3.42 of the Linux
.I man-pages
project.
A description of the project,
and information about reporting bugs,
can be found at
http://www.kernel.org/doc/man-pages/.
